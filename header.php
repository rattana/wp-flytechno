<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fly_Techno
 */

$my_conf = pods('my_configuration');

$request_url = $_SERVER['REQUEST_URI'];
$url_segment = str_replace('/', ' ', $request_url);
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!--<title>Flytechno - Siem Reap</title>-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/preload.min.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/plugins.min.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/style.light-blue-500.min.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/width-boxed.min.css" id="ms-boxed" disabled="">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/assets/css/style.css" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="ms-preload" class="ms-preload">
		<div id="status">
			<div class="spinner">
				<div class="dot1"></div>
				<div class="dot2"></div>
			</div>
		</div>
	</div>

	<div class="sb-site-container">
		<!-- Logo Company - Social Media-->
		<header class="ms-header ms-header-primary">
			<div class="container container-full">
				<!-- Logo Company -->
				<div class="ms-title">
					<a href="/">
						<!-- <img src="assets/img/demo/logo-header.png" alt=""> -->
<!--						<span class="ms-logo animated zoomInDown animation-delay-5">F</span>-->
						<img src="<?php echo $my_conf->display('my_conf_logo'); ?>" alt="flytechno" width="101px" height="71px">
						<!--<h1 class="animated fadeInRight animation-delay-6">Fly
							<span>Techno</span>
						</h1>-->
					</a>
				</div>
				<!-- Social Media -->
				<div class="header-right">
					<div class="share-menu">
						<ul class="share-menu-list">
							<!--<li class="animated fadeInRight animation-delay-3">
								<a href="javascript:void(0)" class="btn-circle btn-google">
									<i class="zmdi zmdi-google"></i>
								</a>
							</li>-->
							<li class="animated fadeInRight animation-delay-2">
								<a href="<?php echo $my_conf->display('my_conf_facebook') ?>" class="btn-circle btn-facebook">
									<i class="zmdi zmdi-facebook"></i>
								</a>
							</li>
							<!--<li class="animated fadeInRight animation-delay-1">
								<a href="javascript:void(0)" class="btn-circle btn-twitter">
									<i class="zmdi zmdi-twitter"></i>
								</a>
							</li>-->
						</ul>
						<a href="javascript:void(0)" class="btn-circle btn-circle-primary animated zoomInDown animation-delay-7">
							<i class="zmdi zmdi-share"></i>
						</a>
					</div>

				</div>
			</div>
		</header>

		<!-- Top Nav bar-->
		<nav class="navbar navbar-static-top yamm ms-navbar ms-navbar-primary">
			<div class="container container-full">
				<div class="navbar-header">
					<a class="navbar-brand" href="/">
						<!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
						<img src="<?php echo $my_conf->display('my_conf_logo'); ?>" alt="flytechno" width="70px" height="40px">
						<!--<span class="ms-logo ms-logo-sm">F</span>
						<span class="ms-title">Fly
							<strong>Techno</strong>
						</span>-->
					</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">

						<li class="dropdown <?php echo is_page( 'Home' ) ? 'active' : ''?>">

							<a href="<?php echo home_url( '/' ) ?>" class="dropdown-toggle animated fadeIn animation-delay-4">Home

							</a>

						</li>

						<li class="dropdown <?php echo is_page( 'Products' ) ? 'active' : ''?>">
							<a href="<?php echo home_url('/products')?>" class="dropdown-toggle animated fadeIn animation-delay-4">Products

							</a>
						</li>

						<li class="dropdown <?php echo is_page( 'Services' ) ? 'active' : ''?>">
							<a href="<?php echo home_url('/services')?>" class="dropdown-toggle animated fadeIn animation-delay-4">Services

							</a>
						</li>

						<li class="dropdown <?php echo is_page( 'Portfolios' ) ? 'active' : ''?>">
							<a href="<?php echo home_url('/portfolios')?>" class="dropdown-toggle animated fadeIn animation-delay-4">Portfolios

							</a>
						</li>

						<li class="dropdown <?php echo is_page( 'Contact' ) ? 'active' : ''?>">
							<a href="<?php echo home_url('/contact')?>" class="dropdown-toggle animated fadeIn animation-delay-4">Contact

							</a>
						</li>
						 <!--<li class="btn-navbar-menu"><a href="javascript:void(0)" class="sb-toggle-left"><i class="zmdi zmdi-menu"></i></a></li>-->
					</ul>
				</div>

				 <!--navbar-collapse collapse -->
				<a href="javascript:void(0)" class="sb-toggle-left btn-navbar-menu">
					<i class="zmdi zmdi-menu"></i>
				</a>
			</div>
			<!-- container -->
		</nav>
