<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fly_Techno
 */

get_header();
?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="ms-hero-page mb-6 ms-hero-bg-primary ms-hero-img-coffee">
            <h2 class="text-center no-m pt-4 pb-4 color-white index-1"><?php the_title(); ?></h2>
<!--            <p class="lead color-white index-1 text-center">Put here the description of your work.</p>-->
        </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="">
                        <div id="carousel-example-generic" class="ms-carousel carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <?php $image_galleries = get_field('image_gallery') ?>
                                <?php $i=0; ?>
                                <?php foreach($image_galleries as $image_gallery) : ?>
                                <div class="item <?php echo $i == 0 ? 'active' : '' ?>">
                                    <img src="<?php echo $image_gallery['url'] ?>" alt="<?php echo $image_gallery['alt'] ?>">
                                    <div class="carousel-caption">
                                        <h3><?php echo $image_gallery['caption'] ?></h3>
                                        <p><?php echo $image_gallery['description'] ?></p>
                                    </div>
                                </div>
                                <?php $i++ ?>
                                <?php endforeach; ?>
                            </div>
                            <!-- Controls -->
                            <a href="#carousel-example-generic" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                                <i class="zmdi zmdi-chevron-left"></i>
                            </a>
                            <a href="#carousel-example-generic" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                                <i class="zmdi zmdi-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="ms-hero-bg-primary ms-hero-img-mountain">
                        <h2 class="text-center no-m pt-4 pb-4 color-white index-1">Information</h2>
                    </div>
                    <div class="card-block">
                        <h3 class="color-primary no-mt">Data</h3>
                        <ul class="list-unstyled">
                            <li>
                                <strong>Title:</strong> <?php the_title() ?></li>
                            <li>
                                <strong>Categories:</strong> <?php the_category() ?>.</li>
                            <li>
                                <strong>Client:</strong> <?php the_field('portfolio_client') ?></li>
                            <li>
                                <strong>Location:</strong> <?php the_field('portfolio_location') ?></li>
                        </ul>
                        <h3 class="color-primary">Description</h3>
                        <p><?php the_field('portfolio_description') ?></p>
                        <!--<p class="text-center">
                            <a href="#" class="btn btn-raised btn-primary">
                                <i class="fa fa-desktop"></i> Live Preview</a>
                        </p>-->
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile; endif; ?>

        <h2 class="right-line mt-6">Related Works</h2>
        <div class="row">
            <?php
                $args = [
                    'post_type'      => 'portfolio',
                    'orderby'        => 'rand',
                    'order'          => 'DESC',
                    'posts_per_page' => 3
                ]
            ?>
            <?php $loop_portfolios = new WP_Query( $args ) ?>
            <?php while ( $loop_portfolios->have_posts() ) : $loop_portfolios->the_post(); ?>
                <div class="col-md-4">
                    <div class="card">
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php the_field('portfolio_image') ;?>" alt="..." class="img-responsive"> </a>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>  <!--./container-->

<?php
get_footer();
