<?php
/**
Template Name: Products
 */
get_header();
?>

    <div class="ms-hero-page ms-hero-img-city2 ms-hero-bg-info mb-6">
        <div class="text-center color-white mt-6 mb-6 index-1">
            <h1>FlyTECHNO</h1>
            <p class="lead lead-lg">Welcome to the Fly TECHNO Store. Discover the latest products at incredible prices.
                <br> Don't forget to check our daily offers.</p>
            <!--<a href="javascript:void(0)" class="btn btn-raised btn-white color-danger">
                <i class="zmdi zmdi-label"></i> Latest offers</a>-->
        </div>
    </div>

    <!---->

    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Filter List</h3>
                    </div>
                    <div class="card-block">
                        <form class="form-horizontal" id="Filters">
                            <h4 class="mb-1 no-mt">Devices</h4>
                            <fieldset>
                                <div class="form-group no-mt">
                                    <?php $added = [] ?>
                                    <?php $loop_products = new WP_Query( array( 'post_type' => 'product', 'posts_per_page' => -1 ) ) ?>
                                    <?php while ( $loop_products->have_posts() ) : $loop_products->the_post(); ?>
                                        <?php if ( !in_array( get_the_category()[0]->slug, $added ) ) : ?>
                                            <div class="checkbox ml-2">
                                                <label>
                                                    <input type="checkbox" value=".<?php echo esc_html(get_the_category()[0]->slug) ?>"> <?php echo esc_html(get_the_category()[0]->name) ?>
                                                </label>
                                            </div>
                                        <?php endif; ?>
                                        <?php $added[] = get_the_category()[0]->slug ?>
                                    <?php endwhile; ?>
                                </div>
                            </fieldset>
                            <button class="btn btn-danger btn-block no-mb mt-2" id="Reset">
                                <i class="zmdi zmdi-delete"></i> Clear Filters</button>
                        </form>
                    </div>
                </div>
            </div>


            <!---->

            <div class="col-md-9">
                <div class="row" id="Container">
                    <?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; ?>
                    <?php $args = [
                        'post_type' => 'product',
                        'posts_per_page' => -1,
                        'orderby' => 'ID',
                        'order' => 'DESC',
                        'paged' => $paged
                    ]
                    ?>
                    <?php $loop_products = new WP_Query( $args ) ?>
                    <?php while ( $loop_products->have_posts() ) : $loop_products->the_post(); ?>
                    <div class="col-lg-4 col-md-6 col-xs-12 mix <?php echo esc_html(get_the_category()[0]->slug) ?>">
                        <div class="card ms-feature">
                            <div class="card-block text-center">
                                <a class="product-image" href="<?php the_permalink(); ?>">
                                    <img src="<?php the_field('image') ?>" alt="" class="img-responsive center-block">
                                </a>
                                <h4 class="text-normal text-center product-title"><?php the_title() ?></h4>
                                <p><?php //echo wp_html_excerpt($loop_products->post->description, 50), '...'; ?></p>
                                <div class="mt-2">
                                    <span class="ms-tag ms-tag-success">$ <?php the_field('price') ?></span>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                    <i class="zmdi zmdi-shopping-cart-plus"></i> View</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <!--<h3>Pagination</h3>
                <nav aria-label="Page navigation">
                    <ul class="pagination pagination-square">
                        <li>
                            <?php
/*                            echo paginate_links( array(
                                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                'total'        => $loop_products->max_num_pages,
                                'current'      => max( 1, get_query_var( 'paged' ) ),
                                'format'       => '?paged=%#%',
                                'show_all'     => false,
                                'type'         => 'plain',
                                'end_size'     => 2,
                                'mid_size'     => 1,
                                'prev_next'    => true,
                                'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                                'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                                'add_args'     => false,
                                'add_fragment' => '',
                            ) );
                            */?>
                        </li>
                    </ul>
                </nav>-->
            </div>

        </div>
    </div>      

<?php
get_footer();
