<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fly_Techno
 */

?>



<?php wp_footer(); ?>

	<!--<aside class="ms-footbar">
		<div class="container">
			<div class="row">
				<div class="col-md-4 ms-footer-col">
					<div class="ms-footbar-block">
						<h3 class="ms-footbar-title">Sitemap</h3>
						<ul class="list-unstyled ms-icon-list three_cols">
							<li>
								<a href="index-2.html">
									<i class="zmdi zmdi-home"></i> Home</a>
							</li>
							<li>
								<a href="page-blog.html">
									<i class="zmdi zmdi-edit"></i> Blog</a>
							</li>
							<li>
								<a href="page-blog.html">
									<i class="zmdi zmdi-image-o"></i> Portafolio</a>
							</li>
							<li>
								<a href="portfolio-filters_sidebar.html">
									<i class="zmdi zmdi-case"></i> Works</a>
							</li>
							<li>
								<a href="page-timeline_left2.html">
									<i class="zmdi zmdi-time"></i> Timeline</a>
							</li>
							<li>
								<a href="page-pricing.html">
									<i class="zmdi zmdi-money"></i> Pricing</a>
							</li>
							<li>
								<a href="page-about.html">
									<i class="zmdi zmdi-favorite-outline"></i> About Us</a>
							</li>
							<li>
								<a href="page-team2.html">
									<i class="zmdi zmdi-accounts"></i> Our Team</a>
							</li>
							<li>
								<a href="page-services.html">
									<i class="zmdi zmdi-face"></i> Services</a>
							</li>
							<li>
								<a href="page-faq2.html">
									<i class="zmdi zmdi-help"></i> FAQ</a>
							</li>
							<li>
								<a href="page-login2.html">
									<i class="zmdi zmdi-lock"></i> Login</a>
							</li>
							<li>
								<a href="page-contact.html">
									<i class="zmdi zmdi-email"></i> Contact</a>
							</li>
						</ul>
					</div>
					<div class="ms-footbar-block">
						<h3 class="ms-footbar-title">Subscribe</h3>
						<p class="">Lorem ipsum Amet fugiat elit nisi anim mollit minim labore ut esse Duis ullamco ad dolor veniam velit.</p>
						<form>
							<div class="form-group label-floating mt-2 mb-1">
								<div class="input-group ms-input-subscribe">
									<label class="control-label" for="ms-subscribe">
										<i class="zmdi zmdi-email"></i> Email Adress</label>
									<input type="email" id="ms-subscribe" class="form-control"> </div>
							</div>
							<button class="ms-subscribre-btn" type="button">Subscribe</button>
						</form>
					</div>
				</div>
				<div class="col-md-5 col-sm-7 ms-footer-col ms-footer-alt-color">
					<div class="ms-footbar-block">
						<h3 class="ms-footbar-title text-center mb-2">Last Articles</h3>
						<div class="ms-footer-media">
							<div class="media">
								<div class="media-left media-middle">
									<a href="javascript:void(0)">
										<img class="media-object media-object-circle" src="<?php /*bloginfo('stylesheet_directory')*/?>/assets/img/demo/p75.jpg" alt="..."> </a>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="javascript:void(0)">Lorem ipsum dolor sit expedita cumque amet consectetur adipisicing repellat</a>
									</h4>
									<div class="media-footer">
							<span>
							  <i class="zmdi zmdi-time color-info-light"></i> August 18, 2016</span>
							<span>
							  <i class="zmdi zmdi-folder-outline color-warning-light"></i>
							  <a href="javascript:void(0)">Design</a>
							</span>
									</div>
								</div>
							</div>
							<div class="media">
								<div class="media-left media-middle">
									<a href="javascript:void(0)">
										<img class="media-object media-object-circle" src="<?php /*bloginfo('stylesheet_directory')*/?>/assets/img/demo/p75.jpg" alt="..."> </a>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="javascript:void(0)">Labore ut esse Duis consectetur expedita cumque ullamco ad dolor veniam velit</a>
									</h4>
									<div class="media-footer">
							<span>
							  <i class="zmdi zmdi-time color-info-light"></i> August 18, 2016</span>
							<span>
							  <i class="zmdi zmdi-folder-outline color-warning-light"></i>
							  <a href="javascript:void(0)">News</a>
							</span>
									</div>
								</div>
							</div>
							<div class="media">
								<div class="media-left media-middle">
									<a href="javascript:void(0)">
										<img class="media-object media-object-circle" src="<?php /*bloginfo('stylesheet_directory')*/?>/assets/img/demo/p75.jpg" alt="..."> </a>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="javascript:void(0)">voluptates deserunt ducimus expedita cumque quaerat molestiae labore</a>
									</h4>
									<div class="media-footer">
							<span>
							  <i class="zmdi zmdi-time color-info-light"></i> August 18, 2016</span>
							<span>
							  <i class="zmdi zmdi-folder-outline color-warning-light"></i>
							  <a href="javascript:void(0)">Productivity</a>
							</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-5 ms-footer-col ms-footer-text-right">
					<div class="ms-footbar-block">
						<div class="ms-footbar-title">
							<span class="ms-logo ms-logo-white ms-logo-sm mr-1">M</span>
							<h3 class="no-m ms-site-title">Material
								<span>Style</span>
							</h3>
						</div>
						<address class="no-mb">
							<p>
								<i class="color-danger-light zmdi zmdi-pin mr-1"></i> 795 Folsom Ave, Suite 600</p>
							<p>
								<i class="color-warning-light zmdi zmdi-map mr-1"></i> San Francisco, CA 94107</p>
							<p>
								<i class="color-info-light zmdi zmdi-email mr-1"></i>
								<a href="mailto:joe@example.com">example@domain.com</a>
							</p>
							<p>
								<i class="color-royal-light zmdi zmdi-phone mr-1"></i>+34 123 456 7890 </p>
							<p>
								<i class="color-success-light fa fa-fax mr-1"></i>+34 123 456 7890 </p>
						</address>
					</div>
					<div class="ms-footbar-block">
						<h3 class="ms-footbar-title">Social Media</h3>
						<div class="ms-footbar-social">
							<a href="javascript:void(0)" class="btn-circle btn-facebook">
								<i class="zmdi zmdi-facebook"></i>
							</a>
							<a href="javascript:void(0)" class="btn-circle btn-twitter">
								<i class="zmdi zmdi-twitter"></i>
							</a>
							<a href="javascript:void(0)" class="btn-circle btn-youtube">
								<i class="zmdi zmdi-youtube"></i>
							</a>
							<br>
							<a href="javascript:void(0)" class="btn-circle btn-google">
								<i class="zmdi zmdi-google"></i>
							</a>
							<a href="javascript:void(0)" class="btn-circle btn-instagram">
								<i class="zmdi zmdi-instagram"></i>
							</a>
							<a href="javascript:void(0)" class="btn-circle btn-github">
								<i class="zmdi zmdi-github"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</aside>-->

	<footer class="ms-footer">
		<div class="container">
			<p>FlyTECHNO - 2018</p>
		</div>
	</footer>
	<div class="btn-back-top">
		<a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
			<i class="zmdi zmdi-long-arrow-up"></i>
		</a>
	</div>


</div><!-- #sb-site-container -->

<div class="ms-slidebar sb-slidebar sb-left sb-style-overlay " id="ms-slidebar" style="margin-left: -300px; transform: translate(300px);">
    <div class="sb-slidebar-container">
        <header class="ms-slidebar-header">
            <div class="ms-slidebar-title">
                <div class="ms-slidebar-t">
                    <!--<span class="ms-logo ms-logo-sm">M</span>-->
                    <h3>Fly
                        <span>TECHNO</span>
                    </h3>
                </div>
            </div>
        </header>
        <ul class="ms-slidebar-menu">
            
            <li>
                <a class="link" href="<?php echo home_url( '/' ) ?>">
                    <i class="zmdi zmdi-home"></i> Home</a>
            </li>
            <li>
                <a class="link" href="<?php echo home_url('/products')?>">
                    <i class="zmdi zmdi-devices"></i> Products</a>
            </li>
            <li>
                <a class="link" href="<?php echo home_url('/services')?>">
                    <i class="zmdi zmdi-group"></i> Services</a>
            </li>
            <li>
                <a class="link" href="<?php echo home_url('/portfolios')?>">
                    <i class="zmdi zmdi-book-image"></i> Portfolios</a>
            </li>
            <li>
                <a class="link" href="<?php echo home_url('/contact')?>">
                    <i class="zmdi zmdi-email"></i> Contact Us</a>
            </li>
        </ul>

    </div>
</div>

<script src="<?php bloginfo('template_directory'); ?>/assets/js/plugins.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/app.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/configurator.min.js"></script>
<script>
	(function(i, s, o, g, r, a, m)
	{
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function()
			{
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-90917746-1', 'auto');
	ga('send', 'pageview');
</script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/portfolio.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/ecommerce.js"></script>



</body>
</html>
