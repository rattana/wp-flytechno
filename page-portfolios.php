<?php
/**
    Template Name: Portfolio
 */
get_header();
?>

    <div class="ms-hero-page ms-hero-img-coffee ms-hero-bg-success mb-6">
        <div class="container">
            <div class="text-center">
                <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Portfolio</h1>
                <p class="lead lead-lg color-white text-center center-block mt-2 mb-4 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7">Discover our projects and the
                    <span class="color-warning">rigorous process</span> of creation. Our principles are creativity, design, experience and knowledge.</p>
                <a href="<?php echo home_url('/services')?>" class="btn btn-raised btn-warning animated fadeInUp animation-delay-10">
                    <i class="zmdi zmdi-accounts"></i> Our Services</a>
                <a href="<?php echo home_url('/contact')?>" class="btn btn-raised btn-info animated fadeInUp animation-delay-10">
                    <i class="zmdi zmdi-email"></i> Concact us</a>
            </div>
        </div>
    </div>

    <!---->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 hidden-sm hidden-xs">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="zmdi zmdi-filter-list"></i>Filter List</h3>
                    </div>
                    <div class="card-block no-pb">
                        <form class="form-horizontal">
                            <h4 class="no-m color-primary">Categories</h4>
                            <div class="form-group mt-1">
                                <div class="radio no-mb">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios0" value="option0" checked="" class="filter" data-filter="all"> All </label>
                                </div>
                                <?php $loop_portfolios = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'post_id', 'order' => 'DESC' ) ) ?>
                                <?php while ( $loop_portfolios->have_posts() ) : $loop_portfolios->the_post(); ?>
                                <div class="radio no-mb">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" class="filter" data-filter=".<?php echo esc_html(get_the_category()[0]->slug) ?>"> <?php echo esc_html(get_the_category()[0]->name) ?> </label>
                                </div>
                                <?php endwhile; ?>
                            </div>
                        </form>
                        <h4 class="mt-2 color-primary no-mb">Columns</h4>
                    </div>
                    <ul class="nav nav-tabs nav-tabs-transparent indicator-primary nav-tabs-full nav-tabs-4" role="tablist">
                        <li>
                            <a id="Cols1" class="withoutripple" href="#home7" aria-controls="home7" role="tab" data-toggle="tab">1</a>
                        </li>
                        <li>
                            <a id="Cols2" class="withoutripple" href="#profile7" aria-controls="profile7" role="tab" data-toggle="tab">2</span>
                            </a>
                        </li>
                        <li class="active">
                            <a id="Cols3" class="withoutripple" href="#messages7" aria-controls="messages7" role="tab" data-toggle="tab">3</a>
                        </li>
                        <li>
                            <a id="Cols4" class="withoutripple" href="#settings7" aria-controls="settings7" role="tab" data-toggle="tab">4</a>
                        </li>
                    </ul>
                    <div class="card-block">
                        <form class="form-horizontal">
                            <div class="form-group no-mt">
                                <div class="col-md-12">
                                    <h4 class="no-m color-primary mb-2">Descriptions</h4>
                                    <div class="togglebutton">
                                        <label>
                                            <input id="port-show" type="checkbox"> Show description </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="row" id="Container">
                    <?php $loop_portfolios = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'post_id', 'order' => 'DESC' ) ) ?>
                    <?php while ( $loop_portfolios->have_posts() ) : $loop_portfolios->the_post(); ?>
                    <div class="col-md-4 col-sm-6 mix <?php echo esc_html(get_the_category()[0]->slug) ?>">
                        <div class="card width-auto">
                            <figure class="ms-thumbnail">
                                <img src="<?php the_field('portfolio_image') ?>" alt="" class="img-responsive">
                                <figcaption class="ms-thumbnail-caption text-center">
                                    <div class="ms-thumbnail-caption-content">
                                        <h4 class="ms-thumbnail-caption-title mb-2"><?php the_title() ?></h4>
                                        <a href="<?php the_permalink(); ?>" class="btn btn-white btn-raised color-primary">
                                            <i class="zmdi zmdi-eye"></i> View more</a>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="card-block text-center portfolio-item-caption hidden">
                                <h3 class="color-primary no-mt"><?php the_title() ?></h3>
                                <p><?php echo wp_html_excerpt($loop_portfolios->post->portfolio_description, 50), '...'; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();