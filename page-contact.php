<?php
/**
Template Name: Contacts
 */

$address_line_1 = get_field('address_line_1');
$address_line_2 = get_field('address_line_2');
$email          = get_field('email');
$phone_1        = get_field('phone_1');
$phone_2        = get_field('phone_2');
$map            = get_field('map');

get_header();
?>

    <div class="ms-hero-page-override ms-hero-img-team ms-hero-bg-primary">
        <div class="container">
            <div class="text-center">
                <h1 class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">Contact Us</h1>
                <p class="lead lead-lg color-light text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7">Do you need a boost in your project?
                    <br>Contact us we will help you to finish your dream.</p>
            </div>
        </div>
    </div>

    <!---->
    <div class="container" style="margin-top: 20px">
        <div class="card card-primary">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-5">
                    <div class="card-block wow fadeInUp">
                        <div class="mb-2">
<!--                            <span class="ms-logo ms-logo-sm mr-1">M</span>-->
                            <h3 class="no-m ms-site-title">Fly
                                <span>TECHNO</span>
                            </h3>
                        </div>
                        <address class="no-mb">
                            <p>
                                <i class="color-danger-light zmdi zmdi-pin mr-1"></i> <?php echo $address_line_1?></p>
                            <p>
                                <i class="color-warning-light zmdi zmdi-map mr-1"></i> <?php echo $address_line_2 ?></p>
                            <p>
                                <i class="color-info-light zmdi zmdi-email mr-1"></i>
                                <a href="mailto:joe@example.com"><?php echo $email ?></a>
                            </p>
                            <p>
                                <i class="color-royal-light zmdi zmdi-phone mr-1"></i><?php echo $phone_1 ?></p>
                            <p>
                                <i class="color-success-light fa fa-fax mr-1"></i><?php echo $phone_2 ?> </p>
                        </address>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-7">
                    <iframe width="100%" height="340" src="<?php echo $map ?>"></iframe>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
