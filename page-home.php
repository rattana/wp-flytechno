<?php
/**
    Template Name: Home Page
 */
$our_service_title = get_field('our_service_title');

// Numerical Data
$numerical_data_title       = get_field('numerical_data_title');

$numerical_data_number_1    = get_field('numerical_data_number_1');
$numerical_data_icon_1      = get_field('numerical_data_icon_1');
$numerical_data_name_1      = get_field('numerical_data_name_1');

$numerical_data_number_2    = get_field('numerical_data_number_2');
$numerical_data_icon_2      = get_field('numerical_data_icon_2');
$numerical_data_name_2      = get_field('numerical_data_name_2');

$numerical_data_number_3    = get_field('numerical_data_number_3');
$numerical_data_icon_3      = get_field('numerical_data_icon_3');
$numerical_data_name_3      = get_field('numerical_data_name_3');

$numerical_data_number_4    = get_field('numerical_data_number_4');
$numerical_data_icon_4      = get_field('numerical_data_icon_4');
$numerical_data_name_4      = get_field('numerical_data_name_4');


get_header();
?>

    <header class="ms-hero ms-hero-black mb-6">
        <div id="carousel-header" class="carousel carousel-header slide" data-ride="carousel" data-interval="5000">
            <!-- Indicators -->
            <?php $count = 0; ?>
            <?php $loop = new WP_Query( array( 'post_type' => 'slider' ) ) ; ?>
            <?php   ?>
            <ol class="carousel-indicators">
                <?php for( $i = 0; $i < $loop->post_count; $i++ ) : ?>
                    <li data-target="#carousel-header" data-slide-to="<?php echo $i ?>" <?php if ($i == 0 )  { echo 'class="active"'; }?>></li>
                <?php endfor; ?>
<!--                <li data-target="#carousel-header" data-slide-to="1"></li>-->
<!--                <li data-target="#carousel-header" data-slide-to="2"></li>-->
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="item <?php if ( $count == 0) { echo 'active' ;} ?> ">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md7">
                                <div class="carousel-caption">
                                    <h1 class="color-primary no-mt mb-4 animated zoomInDown animation-delay-7"><?php the_title(); ?></h1>
                                    <ul class="list-unstyled list-hero">
                                        <?php if ( !empty( $loop->post->text_1 )) : ?>
                                            <li>
                                                <i class="animated flipInX animation-delay-6 color-warning <?php the_field('icon_1') ?>"></i>
                                                <span class="color-warning animated fadeInRightTiny animation-delay-7"><?php the_field('text_1') ?></span>
                                            </li>
                                        <?php endif ?>
                                        <?php if ( !empty( $loop->post->text_2 )) : ?>
                                            <li>
                                                <i class="animated flipInX animation-delay-8 color-info <?php the_field('icon_2') ?>"></i>
                                                <span class="color-info animated fadeInRightTiny animation-delay-9"><?php the_field('text_2') ?></span>
                                            </li>
                                        <?php endif ?>
                                        <?php if ( !empty( $loop->post->text_3 )) : ?>
                                            <li>
                                                <i class="animated flipInX animation-delay-10 color-success <?php the_field('icon_3') ?>"></i>
                                                <span class="color-success animated fadeInRightTiny animation-delay-11"><?php the_field('text_3') ?></span>
                                            </li>
                                        <?php endif ?>
                                    </ul>
                                    <!--<div class="text-center">
                                        <a href="#" class="btn btn-primary btn-xlg btn-raised animated flipInX animation-delay-16">
                                            <i class="zmdi zmdi-settings"></i> Personalize</a>
                                        <a href="#" class="btn btn-warning btn-xlg btn-raised animated flipInX animation-delay-18">
                                            <i class="zmdi zmdi-download"></i> Download</a>
                                    </div>-->
                                </div>
                            </div>
                            <div class="col-lg-6 col-md5">
                                <img src="<?php the_field('slider_image') ?>" alt="..." class="img-responsive mt-6 center-block text-center animated zoomInDown animation-delay-5"> </div>
                        </div>
                    </div>
                </div>
                    <?php $count++ ?>
                <?php endwhile; ?>

            <!-- Controls -->
            <a href="#carousel-header" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary left carousel-control" role="button" data-slide="prev">
                <i class="zmdi zmdi-chevron-left"></i>
            </a>
            <a href="#carousel-header" class="btn-circle btn-circle-xs btn-circle-raised btn-circle-primary right carousel-control" role="button" data-slide="next">
                <i class="zmdi zmdi-chevron-right"></i>
            </a>
        </div>
    </header>

    <!-- Our Services -->
    <div class="container">
        <section class="mb-4">
            <h2 class="text-center no-mt mb-6 wow fadeInUp"><?php echo $our_service_title ?></h2>
            <div class="row">
                <?php $loop_services = new WP_Query( array( 'post_type' => 'our_service' ) ) ?>
                <?php while ( $loop_services->have_posts() ) : $loop_services->the_post(); ?>
                    <div class="col-md-4 col-sm-6 mb-2">
                        <div class="ms-icon-feature wow flipInX animation-delay-4">
                            <div class="ms-icon-feature-icon">
                              <span class="ms-icon ms-icon-lg ms-icon-inverse">
                                <i class="<?php the_field('service_icon') ?>"></i>
                              </span>
                            </div>
                            <div class="ms-icon-feature-content">
                                <h4 class="color-primary"><?php the_title(); ?></h4>
                                <p><?php the_field('service_description') ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </section>
    </div>

    <!-- Numerical Data -->
    <div class="wrap bg-warning color-dark">
        <div class="container">
            <h1 class="color-white text-center mb-4"><?php echo $numerical_data_title ?></h1>
            <div class="row">

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-royal card-block text-center wow zoomInUp animation-delay-2">
                        <h2 class="counter"><?php echo $numerical_data_number_1 ?></h2>
                        <i class="<?php echo $numerical_data_icon_1 ?>"></i>
                        <p class="mt-2 no-mb lead small-caps"><?php echo $numerical_data_name_1 ?></p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-success card-block text-center wow zoomInUp animation-delay-5">
                        <h2 class="counter"><?php echo $numerical_data_number_2 ?></h2>
                        <i class="<?php echo $numerical_data_icon_2 ?>"></i>
                        <p class="mt-2 no-mb lead small-caps"><?php echo $numerical_data_name_2 ?></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-danger card-block text-center wow zoomInUp animation-delay-4">
                        <h2 class="counter"><?php echo $numerical_data_number_3 ?></h2>
                        <i class="<?php echo $numerical_data_icon_3 ?>"></i>
                        <p class="mt-2 no-mb lead small-caps"><?php echo $numerical_data_name_3 ?></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-info card-block text-center wow zoomInUp animation-delay-3">
                        <h2 class="counter"><?php echo $numerical_data_number_4 ?></h2>
                        <i class="<?php echo $numerical_data_icon_4 ?>"></i>
                        <p class="mt-2 no-mb lead small-caps"><?php echo $numerical_data_name_4 ?></p>
                    </div>
                </div>
            </div>
            <!--<div class="text-center color-white mw-800 center-block mt-4">
                <p class="lead lead-lg">Discover our projects and the rigorous process of creation. Our principles are creativity, design, experience and knowledge. We are backed by 20 years of research.</p>
                <a href="javascript:void(0)" class="btn btn-raised btn-white color-info wow flipInX animation-delay-8">
                    <i class="fa fa-space-shuttle"></i> I have a project</a>
            </div>-->
        </div>
    </div>

    <!-- Latest works -->
    <section class="mt-6">
        <div class="container">
            <h1 class="right-line">Latest Works</h1>
            <div class="row">
                <?php $loop_portfolios = new WP_Query( array( 'post_type' => 'portfolio', 'orderby' => 'post_id', 'order' => 'DESC', 'posts_per_page' => 6  ) ) ?>
                <?php while ( $loop_portfolios->have_posts() ) : $loop_portfolios->the_post(); ?>
                    <div class="col-md-4 col-sm-6 mb-4">
                        <div class="ms-thumbnail-container wow fadeInUp">
                            <figure class="ms-thumbnail">
                                <img src="<?php the_field('portfolio_image') ;?>" alt="" class="img-responsive">
                                <figcaption class="ms-thumbnail-caption text-center">
                                    <div class="ms-thumbnail-caption-content">
                                        <h3 class="ms-thumbnail-caption-title"><?php the_title() ?></h3>
                                        <p><?php echo wp_html_excerpt($loop_portfolios->post->portfolio_description, 20), '...'; ?></p>
                                        <a href="<?php the_permalink(); ?>" class="btn btn-white btn-raised color-primary">
                                            <i class="zmdi zmdi-eye"></i> View more</a>
                                    </div>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>

    <!--Pricing Plan-->
    <!--
    <div class="wrap ms-hero-img-airplane ms-hero-bg-royal ms-bg-fixed">
        <div class="container">
            <div class="text-center mb-4">
                <h2 class="uppercase color-white">See our subscription plans</h2>
                <p class="lead uppercase color-light">Surprise with our unique features</p>
            </div>
            <div class="row">
                <div class="col-md-4 price-table price-table-success wow zoomInUp">
                    <header class="price-table-header">
                        <span class="price-table-category">Personal</span>
                        <h3>
                            <sup>$</sup>19
                            <sub>/mo.</sub>
                        </h3>
                    </header>
                    <div class="price-table-body">
                        <ul class="price-table-list">
                            <li>
                                <i class="zmdi zmdi-code"></i> Lorem ipsum dolor sit amet.</li>
                            <li>
                                <i class="zmdi zmdi-globe"></i> Voluptate ex quam autem. Dolor.</li>
                            <li>
                                <i class="zmdi zmdi-settings"></i> Dignissimos velit reiciendis cumque.</li>
                            <li>
                                <i class="zmdi zmdi-cloud"></i> Nihil corrupti soluta vitae non.</li>
                            <li>
                                <i class="zmdi zmdi-star"></i> Atque molestiae, blanditiis ratione.</li>
                        </ul>
                        <div class="text-center">
                            <a href="javascript:void(0)" class="btn btn-success btn-raised">
                                <i class="zmdi zmdi-cloud-download"></i> Get Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 price-table price-table-info prominent wow zoomInDown">
                    <header class="price-table-header">
                        <span class="price-table-category">Professional</span>
                        <h3>
                            <sup>$</sup>49
                            <sub>/mo.</sub>
                        </h3>
                    </header>
                    <div class="price-table-body">
                        <ul class="price-table-list">
                            <li>
                                <i class="zmdi zmdi-code"></i> Lorem ipsum dolor sit amet.</li>
                            <li>
                                <i class="zmdi zmdi-globe"></i> Voluptate ex quam autem. Dolor.</li>
                            <li>
                                <i class="zmdi zmdi-settings"></i> Dignissimos velit reiciendis cumque.</li>
                            <li>
                                <i class="zmdi zmdi-cloud"></i> Nihil corrupti soluta vitae non.</li>
                            <li>
                                <i class="zmdi zmdi-star"></i> Atque molestiae, blanditiis ratione.</li>
                        </ul>
                        <div class="text-center">
                            <a href="javascript:void(0)" class="btn btn-info btn-raised">
                                <i class="zmdi zmdi-cloud-download"></i> Get Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 price-table price-table-danger wow zoomInUp">
                    <header class="price-table-header">
                        <span class="price-table-category">Business</span>
                        <h3>
                            <sup>$</sup>99
                            <sub>/mo.</sub>
                        </h3>
                    </header>
                    <div class="price-table-body">
                        <ul class="price-table-list">
                            <li>
                                <i class="zmdi zmdi-code"></i> Lorem ipsum dolor sit amet.</li>
                            <li>
                                <i class="zmdi zmdi-globe"></i> Voluptate ex quam autem. Dolor.</li>
                            <li>
                                <i class="zmdi zmdi-settings"></i> Dignissimos velit reiciendis cumque.</li>
                            <li>
                                <i class="zmdi zmdi-cloud"></i> Nihil corrupti soluta vitae non.</li>
                            <li>
                                <i class="zmdi zmdi-star"></i> Atque molestiae, blanditiis ratione.</li>
                        </ul>
                        <div class="text-center">
                            <a href="javascript:void(0)" class="btn btn-danger btn-raised">
                                <i class="zmdi zmdi-cloud-download"></i> Get Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        container
    </div>
    -->
    <!--Our team-->
    <!--
    <section class="mt-6">
        <div class="container">
            <h1 class="color-primary text-center" text-center>Our Team</h1>
            <div class="col-md-4 col-sm-6">
                <div class="card mt-4 card-danger wow zoomInUp">
                    <div class="ms-hero-bg-danger ms-hero-img-city">
                        <img src="<?php bloginfo('stylesheet_directory')?>/assets/img/demo/avatar1.jpg" alt="..." class="img-avatar-circle"> </div>
                    <div class="card-block pt-6 text-center">
                        <h3 class="color-danger">Victoria Smith</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur alter adipisicing elit. Facilis, natuse inse voluptates officia repudiandae beatae magni es magnam autem molestias.</p>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook">
                            <i class="zmdi zmdi-facebook"></i>
                        </a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter">
                            <i class="zmdi zmdi-twitter"></i>
                        </a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram">
                            <i class="zmdi zmdi-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="card mt-4 card-info wow zoomInUp">
                    <div class="ms-hero-bg-info ms-hero-img-city">
                        <img src="<?php bloginfo('stylesheet_directory')?>/assets/img/demo/avatar2.jpg" alt="..." class="img-avatar-circle"> </div>
                    <div class="card-block pt-6 text-center">
                        <h3 class="color-info">Charlie Durant</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur alter adipisicing elit. Facilis, natuse inse voluptates officia repudiandae beatae magni es magnam autem molestias.</p>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook">
                            <i class="zmdi zmdi-facebook"></i>
                        </a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter">
                            <i class="zmdi zmdi-twitter"></i>
                        </a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram">
                            <i class="zmdi zmdi-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="card mt-4 card-warning wow zoomInUp">
                    <div class="ms-hero-bg-warning ms-hero-img-city">
                        <img src="<?php bloginfo('stylesheet_directory')?>/assets/img/demo/avatar3.jpg" alt="..." class="img-avatar-circle"> </div>
                    <div class="card-block pt-6 text-center">
                        <h3 class="color-warning">Joan Fawert</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur alter adipisicing elit. Facilis, natuse inse voluptates officia repudiandae beatae magni es magnam autem molestias.</p>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-facebook">
                            <i class="zmdi zmdi-facebook"></i>
                        </a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-twitter">
                            <i class="zmdi zmdi-twitter"></i>
                        </a>
                        <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-xs mt-1 mr-1 no-mr-md btn-instagram">
                            <i class="zmdi zmdi-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->
<?php
get_footer();
