<?php
/**
Template Name: Service
 */
get_header();
?>

    <header class="ms-hero-page ms-hero-bg-dark-light ms-hero-img-city2 mb-4 ms-bg-fixed">
        <div class="container">
            <div class="text-center">
                <h1 class="color-info mb-4 animated fadeInDown animation-delay-4">Services</h1>
                <p class="lead lead-xl mw-800 center-block color-medium mb-2 animated fadeInDown animation-delay-4"> We are at the
                    <span class="colorStar">forefront of innovation</span>.
                    <br> Discover with us the possibilities of
                    <span class="colorStar animation-delay-10">your next project</span>. </p>
            </div>
            <div class="row mt-4">
                <?php $loop_services = new WP_Query( array( 'post_type' => 'our_service' ) ) ?>
                <?php while ( $loop_services->have_posts() ) : $loop_services->the_post(); ?>
                <div class="col-md-4 col-sm-6">
                    <div class="ms-icon-feature wow flipInX animation-delay-4">
                        <div class="ms-icon-feature-icon">
                  <span class="ms-icon ms-icon-lg ms-icon-inverse ms-icon-white">
                    <i class="<?php the_field('service_icon') ?>"></i>
                  </span>
                        </div>
                        <div class="ms-icon-feature-content color-white">
                            <h4 class="color-info"><?php the_title() ?></h4>
                            <p> <?php the_field('service_description') ?> </p>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="text-center">
                <a href="<?php echo home_url('/portfolios')?>" class="btn btn-raised btn-white color-royal animated fadeInUp animation-delay-10">
                    <i class="zmdi zmdi-desktop-mac"></i> Portfolio </a>
                <a href="<?php echo home_url('/contact')?>" class="btn btn-raised btn-white color-success animated fadeInUp animation-delay-10">
                    <i class="zmdi zmdi-email"></i> Concact us</a>
            </div>
        </div>
    </header>

    <!---->
    <!--
    <div class="container">
        <h1 class="right-line">Our Values</h1>
        <div class="row">
            <div class="col-sm-6">
                <ol class="service-list list-unstyled">
                    <li>Lorem ipsum dolor sit amet,
                        <strong>consectetur adipisicing elit</strong>. Nihil suscipit cupiditate expedita hic earum vero sint, recusandae itaque, rem distinctio.</li>
                    <li>Totam porro sit, obcaecati quos quae iure tenetur, soluta voluptatem sapiente rerum ipsam delectus corporis voluptates voluptate, nulla mollitia pariatur.</li>
                    <li>Amet dolorum ullam, rerum ratione distinctio, quia iusto rem! Asperiores et quas, ratione in dolores dolorum doloribus magni suscipit labore!</li>
                    <li>Enim quas nesciunt sequi odit, ut
                        <a href="#">quisquam vitae commodi</a> animi placeat nihil saepe magnam aliquam, vero harum quae doloribus aut nostrum veniam alias!</li>
                    <li>Expedita doloribus vel nam fuga iusto aperiam maxime aut amet pariatur. Libero quidem, optio itaque ducimus. Nulla laboriosam voluptas voluptates.</li>
                    <li>Amet dolorum ullam, rerum ratione distinctio, quia iusto rem! Asperiores et quas, ratione in dolores dolorum doloribus magni suscipit labore!</li>
                    <li>Lorem ipsum dolor sit amet,
                        <strong>consectetur adipisicing elit</strong>. Nihil suscipit cupiditate expedita hic earum vero sint, recusandae itaque, rem distinctio.</li>
                </ol>
            </div>
            <div class="col-sm-6">
                <div class="card card-primary mb-4">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="text-center center-block">
                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/img/demo/office1.jpg" alt="" class="img-responsive"> </div>
                            </div>
                            <div class="col-md-8 text-center">
                                <h4 class="color-primary no-mt-md">Lorem ipsum dolor sit amet</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, alias similique sapiente rerum ipsam delectus corporis.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary-inverse mb-4">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="text-center center-block">
                                    <img src="assets/img/demo/office3.jpg" alt="" class="img-responsive"> </div>
                            </div>
                            <div class="col-md-8 text-center">
                                <h4 class="no-mt-md">Lorem ipsum dolor sit amet</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, alias similique sapiente rerum ipsam delectus corporis.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary mb-4">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="text-center center-block">
                                    <img src="assets/img/demo/office2.jpg" alt="" class="img-responsive"> </div>
                            </div>
                            <div class="col-md-8 text-center">
                                <h4 class="color-primary no-mt-md">Lorem ipsum dolor sit amet</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, alias similique sapiente rerum ipsam delectus corporis.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary-inverse mb-4">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="text-center center-block">
                                    <img src="assets/img/demo/office4.jpg" alt="" class="img-responsive"> </div>
                            </div>
                            <div class="col-md-8 text-center">
                                <h4 class="no-mt-md">Lorem ipsum dolor sit amet</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, alias similique sapiente rerum ipsam delectus corporis.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->
<?php
get_footer();
