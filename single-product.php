<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fly_Techno
 */

get_header();
?>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div id="carousel-product" class="ms-carousel ms-carousel-thumb carousel slide animated zoomInUp animation-delay-5" data-ride="carousel" data-interval="0">
                    <div class="card card-block">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php $image_galleries = get_field('image_gallery'); ?>
                            <?php $i=0; ?>
                            <?php foreach($image_galleries as $image_gallery) : ?>
                            <div class="item <?php echo $i == 0 ? 'active' : '' ?>">
                                <img src="<?php echo $image_gallery['url'] ?>" alt="<?php echo $image_gallery['alt'] ?>">
                            </div>
                            <?php $i++ ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- Indicators -->
                    <ol class="carousel-indicators carousel-indicators-tumbs carousel-indicators-tumbs-outside">
                        <?php $j=0; ?>
                        <?php foreach($image_galleries as $image_gallery) : ?>
                            <li data-target="#carousel-product" data-slide-to="<?php echo $j; ?>" class="<?php echo $j == 0 ? 'active' : '' ?>" style="width:80px; height:80px">
                                <img src="<?php echo $image_gallery['url'] ?>" alt="<?php echo $image_gallery['alt'] ?> " >
                            </li>
                        <?php $j++ ?>
                        <?php endforeach; ?>
                    </ol>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card animated zoomInDown animation-delay-5">
                    <div class="card-block">
                        <h2><?php the_title() ?></h2>
                        <div class="mb-2 mt-4">
                            <div class="row">
                                <div class="col-sm-6">

                                </div>
                                <div class="col-sm-6 text-center">
                                    <h2 class="color-success no-m text-normal">$ <?php the_field('price') ?></h2>
                                </div>
                            </div>
                        </div>
                        <p class="lead"><?php the_field('description') ?>.</p>


                    </div>
                </div>
            </div>
        </div>
        <h2 class="mt-4 mb-4 right-line">Related Products</h2>
        <div class="row">
            <?php
            $args = [
                'post_type'      => 'product',
                'orderby'        => 'rand',
                'order'          => 'DESC',
                'posts_per_page' => 3
            ]
            ?>
            <?php $loop_products = new WP_Query( $args ) ?>
            <?php while ( $loop_products->have_posts() ) : $loop_products->the_post(); ?>
            <div class="col-md-4">
                <div class="card ms-feature wow zoomInUp animation-delay-3">
                    <div class="card-block text-center">
                        <a class="related-product-image" href="<?php the_permalink(); ?>">
                            <img src="<?php the_field('image') ?>" alt="" class="img-responsive center-block">
                        </a>
                        <h4 class="text-normal text-center"><?php the_title() ?></h4>
                        <p><?php //echo wp_html_excerpt($loop_products->post->description, 50), '...'; ?></p>
                        <div class="mt-2">
                            <span class="ms-tag ms-tag-success">$ <?php the_field('price') ?></span>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                            <i class="zmdi zmdi-shopping-cart-plus"></i> View</a>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>

<?php
get_footer();
